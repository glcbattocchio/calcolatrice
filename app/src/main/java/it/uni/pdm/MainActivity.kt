package it.uni.pdm

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.github.jairrab.calc.Calculator
import com.github.jairrab.calc.CalculatorType
import kotlin.math.log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val calcolatrice = Calculator.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        collegaBottoniToListener()
        //todo migliorare il comportamento della label operazioni quando si riempe
        //todo risolvere il problema dei margini visibili tra le righe del tastierino numerico
        //todo migliorare grafica modalita orizzontale
        //todo divisione per 0 gestita ma nessuna notifica all'utente. aggiungerla

    }

    private fun collegaBottoniToListener()
    {
        btn_zero.setOnClickListener(this)
        btn_one.setOnClickListener(this)
        btn_two.setOnClickListener(this)
        btn_three.setOnClickListener(this)
        btn_four.setOnClickListener(this)
        btn_five.setOnClickListener(this)
        btn_six.setOnClickListener(this)
        btn_seven.setOnClickListener(this)
        btn_eight.setOnClickListener(this)
        btn_nine.setOnClickListener(this)

        btn_dot.setOnClickListener(this)
        btn_undo.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
        btn_AC.setOnClickListener(this)

        btn_plus.setOnClickListener(this)
        btn_minus.setOnClickListener(this)
        btn_star.setOnClickListener(this)
        btn_slash.setOnClickListener(this)

        //Il tasto percentuale c'è solo nella modalità orizzontale.
        //Senza questo controllo genera una nullpointerexception
        if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            btn_percent.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.btn_zero ->{
                calcolatrice.pressZero()
                aggiornaLabelRisultati("0")
            }
            R.id.btn_one ->{
                //Toast.makeText(this, "This is my Toast message!", Toast.LENGTH_LONG).show();
                calcolatrice.pressOne()
                aggiornaLabelRisultati("1")
            }
            R.id.btn_two->{
                calcolatrice.pressTwo()
                aggiornaLabelRisultati("2")
            }
            R.id.btn_three ->{
                calcolatrice.pressThree()
                aggiornaLabelRisultati("3")
            }
            R.id.btn_four ->{
                calcolatrice.pressFour()
                aggiornaLabelRisultati("4")
            }
            R.id.btn_five ->{
                calcolatrice.pressFive()
                aggiornaLabelRisultati("5")
            }
            R.id.btn_six ->{
                calcolatrice.pressSix()
                aggiornaLabelRisultati("6")
            }
            R.id.btn_seven ->{
                calcolatrice.pressSeven()
                aggiornaLabelRisultati("7")
            }
            R.id.btn_eight ->{
                calcolatrice.pressEight()
                aggiornaLabelRisultati("8")
            }
            R.id.btn_nine ->{
                calcolatrice.pressNine()
                aggiornaLabelRisultati("9")
            }

            R.id.btn_dot ->{
                calcolatrice.pressDecimal()
                aggiornaLabelRisultati(".")
            }

            R.id.btn_plus ->{
                calcolatrice.pressPlus()
                aggiornaLabelRisultati("+")
            }
            R.id.btn_minus ->{
                calcolatrice.pressMinus()
                aggiornaLabelRisultati("-")
            }
            R.id.btn_star ->{
                calcolatrice.pressMultiply()
                aggiornaLabelRisultati("*")
            }
            R.id.btn_slash ->{
                calcolatrice.pressDivide()
                aggiornaLabelRisultati("/")
            }
            R.id.btn_percent ->{
                calcolatrice.pressPercent()
                aggiornaLabelRisultati("%")
            }

            R.id.btn_undo ->{
                calcolatrice.backSpace()
                val tmp = txv_operations.text.toString()
                txv_operations.text = tmp.substring(0, if (tmp.isNotEmpty()) tmp.length-1 else 0)


                txv_result.text = calcolatrice.getCurrentNumber().toString()
            }

            R.id.btn_AC ->{
                calcolatrice.clear()
                txv_operations.text = ""
                txv_result.text = calcolatrice.getCurrentNumber().toString()
            }

            R.id.btn_submit ->{
                txv_result.text = calcolatrice.getCurrentNumber().toString()
                txv_operations.text = ""
                calcolatrice.clear()
            }

        }
    }

    private fun aggiornaLabelRisultati(simbolo : String)
    {
        val tmp = txv_operations.text.toString()
        txv_operations.text = tmp + simbolo

        txv_result.text = calcolatrice.getCurrentNumber().toString()
    }
}